﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MonsterCtrl2 : MonoBehaviour {

    public enum MonsterState { idle, trace, attack, die };

    public MonsterState monsterState = MonsterState.idle;

    private Transform monsterTr;
    private Transform playerTr;
    private NavMeshAgent nvAgent;
    private Animator animator;

    public float traceDist = 30.0f;
    public float attackDist = 10.0f;
    public float patrolDist = 3.0f;

    public GameObject bloodEffect;
    public GameObject bloodDecal;

    public GameObject bullet;
    public Transform firePos;
    public AudioClip fireSound;
    private bool canAttack = true;
    public float coolTime;

    //public GameObject prevPoint;
    //public GameObject patrolPoint;
    //private bool toPrevPoint = false;
    //private bool toPatrolPoint = true;

    public int hp = 20;
    public bool isDie = false;

    private GameUI gameUI;

    //void Start()
    //{
    //    monsterTr = gameObject.GetComponent<Transform>();
    //    playerTr = GameObject.FindWithTag("Player").GetComponent<Transform>();
    //    nvAgent = gameObject.GetComponent<NavMeshAgent>();
    //    animator = gameObject.GetComponent<Animator>();
    //    gameUI = GameObject.Find("GameUI").GetComponent<GameUI>();

    //    StartCoroutine(CheckMonsterState());
    //    StartCoroutine(MonsterAction());
    //}
    void Awake()
    {
        monsterTr = gameObject.GetComponent<Transform>();
        playerTr = GameObject.FindWithTag("Player").GetComponent<Transform>();
        nvAgent = gameObject.GetComponent<NavMeshAgent>();
        animator = gameObject.GetComponent<Animator>();
        gameUI = GameObject.Find("GameUI").GetComponent<GameUI>();
    }

    void OnEnable()
    {
        //PlayerCtrl.OnPlayerDie += this.OnPlayerDie;

        StartCoroutine(this.CheckMonsterState());
        StartCoroutine(this.MonsterAction());
    }

    // Update is called once per frame
    void Update () {
		
	}

    IEnumerator CheckMonsterState()
    {
        while (!isDie)
        {
            yield return new WaitForSeconds(0.2f);
            float dist = Vector3.Distance(playerTr.position, monsterTr.position);
            
            if (dist <= attackDist)
            {
                monsterState = MonsterState.attack;
            }
            else if(dist <= traceDist)
            {
                monsterState = MonsterState.trace;
            }
            //else if(dist > traceDist && toPatrolPoint)
            //{
            //    monsterState = MonsterState.patrolpoint;
            //}
            //else if(dist > traceDist && toPrevPoint)
            //{
            //    monsterState = MonsterState.patrolprev;
            //}
            else
            {
                monsterState = MonsterState.idle;
            }
        }
    }

    IEnumerator MonsterAction()
    {
        while (!isDie)
        {
            switch (monsterState)
            {
                case MonsterState.idle:
                    nvAgent.Stop();
                    animator.SetBool("IsTrace", false);
                    break;
                case MonsterState.trace:
                    nvAgent.Resume();
                    nvAgent.destination = playerTr.position;
                    animator.SetBool("IsAttack", false);
                    animator.SetBool("IsTrace", true);
                    break;
                case MonsterState.attack:
                    nvAgent.Stop();
                    animator.SetBool("IsAttack", true);
                    Fire();      
                    break;
                //case MonsterState.patrolpoint:
                //    nvAgent.destination = patrolPoint.transform.position;
                //    animator.SetBool("IsAttack", false);
                //    animator.SetBool("IsTrace", true);
                //    if(Vector3.Distance(monsterTr.position, patrolPoint.transform.position) <= 2.0f)
                //    {
                //        toPatrolPoint = false;
                //        toPrevPoint = true;
                //    }
                //    break;
                //case MonsterState.patrolprev:
                //    nvAgent.destination = prevPoint.transform.position;
                //    animator.SetBool("IsAttack", false);
                //    animator.SetBool("IsTrace", true);
                //    if (Vector3.Distance(monsterTr.position, prevPoint.transform.position) <= 2.0f)
                //    {
                //        toPrevPoint = false;
                //        toPatrolPoint = true;
                //    }
                //    break;
            }
            yield return null;
        }
    }

    void OnCollisionEnter(Collision coll)
    {
        if(coll.gameObject.tag == "BULLET")
        {
            CreateBloodEffect(coll.transform.position);

            hp -= coll.gameObject.GetComponent<BulletCtrl>().damage;
            if(hp <= 0)
            {
                MonsterDie();
            }

            Destroy(coll.gameObject);
            animator.SetTrigger("IsHit");
        }

        if (coll.gameObject.tag == "Missile")
        {
            CreateBloodEffect(coll.transform.position);

            hp -= coll.gameObject.GetComponent<BulletCtrl>().damage;
            if (hp <= 0)
            {
                MonsterDie();
            }

            Destroy(coll.gameObject);
            animator.SetTrigger("IsHit");
        }
    }

    void MonsterDie()
    {
        gameObject.tag = "Untagged";

        StopAllCoroutines();
        nvAgent.Stop();
        isDie = true;
        monsterState = MonsterState.die;
        animator.SetTrigger("IsDie");

        gameObject.GetComponent<CapsuleCollider>().enabled = false;

        foreach(Collider coll in gameObject.GetComponentsInChildren<SphereCollider>())
        {
            coll.enabled = false;
        }

        gameUI.DispGold(100);
        gameUI.DispCount(1);
       

        StartCoroutine(this.PushObjectPool());
    }

    void CreateBloodEffect(Vector3 pos)
    {
        GameObject blood1 = (GameObject)Instantiate(bloodEffect, pos, Quaternion.identity);
        Destroy(blood1, 2.0f);

        Vector3 decalPos = monsterTr.position + (Vector3.up * 0.05f);
        Quaternion decalRot = Quaternion.Euler(90, 0, Random.Range(0, 360));

        GameObject blood2 = (GameObject)Instantiate(bloodDecal, decalPos, decalRot);
        float scale = Random.Range(1.5f, 3.5f);
        blood2.transform.localScale = Vector3.one * scale;

        Destroy(blood2, 5.0f);
    }

    void OnPlayerDie()
    {
        nvAgent.Stop();
        StopAllCoroutines();

        animator.SetTrigger("IsPlayerDie");
    }

    IEnumerator PushObjectPool()
    {
        yield return new WaitForSeconds(3.0f);

        isDie = false;
        hp = 20;
        gameObject.tag = "MONSTER";
        monsterState = MonsterState.idle;

        gameObject.GetComponentInChildren<CapsuleCollider>().enabled = true;

        foreach (Collider coll in gameObject.GetComponentsInChildren<SphereCollider>())
        {
            coll.enabled = true;
        }

        gameObject.SetActive(false);
    }

    void Fire()
    {
        if (canAttack == true)
        {
            StartCoroutine("CoolTime");

            canAttack = false;

            CreateBullet();
            AudioSource.PlayClipAtPoint(fireSound, firePos.position);
        }
        else
        {

        }
    }

    void CreateBullet()
    {
        Instantiate(bullet, firePos.position, firePos.rotation);
    }

    IEnumerator CoolTime()
    {
        float i = coolTime;

        while (i > 0)
        {
            i -= 1 * Time.smoothDeltaTime;
            yield return null;
        }

        canAttack = true;
        yield break;
    }

}

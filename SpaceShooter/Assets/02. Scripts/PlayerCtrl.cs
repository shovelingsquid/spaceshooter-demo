﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Anim
{
    public AnimationClip idle;
    public AnimationClip runForward;
    public AnimationClip runBackWard;
    public AnimationClip runRight;
    public AnimationClip runLeft;
}

public class PlayerCtrl : MonoBehaviour {

    private float h = 0.0f;
    private float v = 0.0f;
    private float ch = 0.0f;

    private Transform tr;
    public float moveSpeed = 10.0f;
    public float rotSpeed = 100.0f;

    public Anim anim;
    public Animation _animation;

    Vector3 hitPosition;

    Vector3 PointPos;

    private bool KeyPress = false;

    public int hp = 80;
    private int initHp;
    public Image imgHpbar;

    private GameMgr gameMgr;

    Vector3 v3;

    public JoyStickMove joyStick;
    public JoyStickCameraMove joyStickCamera;

    public FireCtrl AutoShot;
    RaycastHit hitObject;
    float fireTime = 0.0f;
    public float distance = 30.0f;

    private UIMgr UIDATA;
    
    // Use this for initialization
    void Start () {
        initHp = hp;

        tr = GetComponent<Transform>();

        gameMgr = GameObject.Find("GameManager").GetComponent<GameMgr>();

        _animation = GetComponentInChildren<Animation>();
        _animation.clip = anim.idle;
        _animation.Play();

        UIDATA = GameObject.Find("UIManager").GetComponent<UIMgr>();

        distance = UIDATA.distance;
        moveSpeed = UIDATA.speed;
    }
	
	// Update is called once per frame
	void Update () {
        //h = Input.GetAxis("Horizontal");
        //v = Input.GetAxis("Vertical"); 

        h = joyStick.Horizontal();
        v = joyStick.Vertical();
        ch = joyStickCamera.Horizontal();

        fireTime += Time.deltaTime;

        //Debug.Log("H=" + h.ToString());
        //Debug.Log("V=" + v.ToString());

        //키보드 이동
        Vector3 moveDir = (Vector3.forward * v) + (Vector3.right * h);
        tr.Translate(moveDir.normalized * moveSpeed * Time.deltaTime, Space.Self);
        //카메라 위치
        v3 = new Vector3(0, ch * 1.5f, 0);
        tr.Rotate(v3 * rotSpeed * Time.deltaTime);

        if (v >= 0.1f)
        {
            _animation.CrossFade(anim.runForward.name, 0.3f);
        }
        else if (v <= -0.1f)
        {
            _animation.CrossFade(anim.runBackWard.name, 0.3f);
        }
        else if (h >= 0.1f)
        {
            _animation.CrossFade(anim.runRight.name, 0.3f);
        }
        else if (h <= -0.1f)
        {
            _animation.CrossFade(anim.runLeft.name, 0.3f);
        }
        else
        {
            _animation.CrossFade(anim.idle.name, 0.3f);
        }

        if(fireTime > 0.3f)
        {
            if (Physics.Raycast(AutoShot.firePos.transform.position, AutoShot.firePos.transform.forward, out hitObject, distance))
            {
                if (hitObject.collider.tag == "MONSTER")
                {
                    AutoShot.Fire();
                    fireTime = 0.0f;
                }
            }
        }
    }

    void OnTriggerEnter(Collider coll)
    {
        if(coll.gameObject.tag == "PUNCH")
        {
            hp -= 10;
            imgHpbar.fillAmount = (float)hp / (float)initHp;
            if(hp <= 0)
            {
                PlayerDie();
            }
        }
        else if(coll.gameObject.tag == "BPUNCH")
        {
            hp -= 20;
            imgHpbar.fillAmount = (float)hp / (float)initHp;
            if (hp <= 0)
            {
                PlayerDie();
            }
        }
        else if (coll.gameObject.tag == "MBULLET")
        {
            hp -= coll.gameObject.GetComponent<MonsterBulletCtrl>().damage;
            imgHpbar.fillAmount = (float)hp / (float)initHp;

            if (hp <= 0)
            {
                PlayerDie();
            }

            Destroy(coll.gameObject);
        }

    }

    //void OnCollisionEnter(Collision coll)
    //{
    //    if (coll.gameObject.tag == "MBULLET")
    //    {
    //        hp -= coll.gameObject.GetComponent<MonsterBulletCtrl>().damage;
    //        imgHpbar.fillAmount = (float)hp / (float)initHp;

    //        if (hp <= 0)
    //        {
    //            PlayerDie();
    //        }

    //        Destroy(coll.gameObject);
    //    }
    //}

    void PlayerDie()
    {
        //Debug.Log("Player Die !!");

        GameObject[] monsters = GameObject.FindGameObjectsWithTag("MONSTER");

        foreach(GameObject monster in monsters)
        {
            monster.SendMessage("OnPlayerDie", SendMessageOptions.DontRequireReceiver);
        }

        gameMgr.isGameOver = true;
    }

    //IEnumerator Run()
    //{
    //    while (true)
    //    {
    //        _animation.CrossFade(anim.runForward.name, 0.3f);
    //        if (Vector3.Distance(transform.position, PointPos) < 2f)
    //            break;

    //        Vector3 vec = (PointPos - transform.position).normalized;

    //        Quaternion q = Quaternion.LookRotation(vec);
    //        q.x = 0;
    //        q.z = 0;

    //        transform.rotation = Quaternion.Slerp(transform.rotation, q, rotSpeed * Time.deltaTime);
    //        transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);

    //        yield return null;
    //    }
    //    _animation.CrossFade(anim.idle.name, 0.3f);
    //}

}

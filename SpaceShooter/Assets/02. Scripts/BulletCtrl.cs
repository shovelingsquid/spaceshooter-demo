﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCtrl : MonoBehaviour {
    public int damage = 20;

    public float speed = 1000.0f;
    // Use this for initialization

    private UIMgr UIDATA;

    void Start () {
        UIDATA = GameObject.Find("UIManager").GetComponent<UIMgr>();

        damage = UIDATA.attack;
        GetComponent<Rigidbody>().AddForce(transform.forward * speed);
	}
	
	// Update is called once per frame
}

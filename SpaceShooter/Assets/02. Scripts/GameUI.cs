﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUI : MonoBehaviour {

    public Text monsterCount;
    public Text bulletCount;
    public Text goldCount;

    public Transform Cameratr;
    private Vector3 FPSpos;
    private Vector3 TPSpos;
    private bool isFPSON;
    private bool isTPSON;

    public int addMonster = 2;
    public int monCount = 2;
    private int mCount = 2;
    public int maxCount = 2;

    private int bCount = 0;
    private int gCount = 0;

    public int waveNum = 1;

    public bool isPause = false;
    public bool isBoss = false;

    private UIMgr UIDATA;
    private GameMgr GAMEDATA;

    // Use this for initialization
    void Start () {
        UIDATA = GameObject.Find("UIManager").GetComponent<UIMgr>();
        GAMEDATA = GameObject.Find("GameManager").GetComponent<GameMgr>();

        isFPSON = false;
        isTPSON = true;

        DispCount(0);
        DispBulletCount(UIDATA.magazine);
        DispGold(0);
    }
	
	// Update is called once per frame
	void Update () {
        if(Input.GetKey(KeyCode.Escape))
        {
            if (isPause)
            {
                Time.timeScale = 1;
                isPause = false;
            }
            else if (!isPause)
            {
                Time.timeScale = 0;
                isPause = true;
            }
        }
    }

    public void DispCount(int m)
    {
        mCount -= m;
        monsterCount.text = "Monster <color=#ff0000>" + mCount.ToString() + "</color> / <color=#ff0000>" + maxCount.ToString() + "</color>";

        if(mCount == 0)
        {
            WaveOver();
        }
    }
    public void DispCountp()
    {
        mCount++;
        monsterCount.text = "Monster <color=#ff0000>" + mCount.ToString() + "</color> / <color=#ff0000>" + maxCount.ToString() + "</color>";
    }
    public void DispBulletCount(int bullet)
    {
        bCount = bullet;
        bulletCount.text = "Bullet <color=#ff0000>" + bCount.ToString() + "</color>";
    }
    public void DispGold(int gold)
    {
        gCount += gold;
        goldCount.text = "Gold <color=#e8f442>" + gCount.ToString() + "</color>";
    }
    public void OnMenuClick()
    {
        Debug.Log("Click MenuBtn");
        if (isPause)
        {
            Time.timeScale = 1;
            isPause = false;
        }
        else if (!isPause)
        {
            Time.timeScale = 0;
            isPause = true;
        }
    }
    public void OnClickFPSBtn()
    {
        //Debug.Log("FPSBtn Clicked");
        if (isTPSON && !isFPSON)
        {
            FPSpos = Cameratr.position;
            FPSpos.x -= 0.2f;   FPSpos.y -= 0.5f; FPSpos.z += 1.43f;
            Cameratr.transform.position = FPSpos;
            isTPSON = false;
            isFPSON = true;
            Time.timeScale = 1;
            isPause = false;
        }
    }
    public void OnClickTPSBtn()
    {
        //Debug.Log("TPSBtn Clicked");
        if(!isTPSON && isFPSON)
        {
            TPSpos = Cameratr.position;
            TPSpos.x += 0.2f; TPSpos.y += 0.5f; TPSpos.z -= 1.43f;
            Cameratr.transform.position = TPSpos;
            isTPSON = true;
            isFPSON = false;
            Time.timeScale = 1;
            isPause = false;
        }
    }

    public void WaveOver()
    {
        if (waveNum == 5)
        {
            isBoss = true;
            return;
        }

        if (waveNum < 5)
        {
            waveNum++;
            maxCount += addMonster;
            mCount = maxCount;

            DispCount(0);

            StartCoroutine(GAMEDATA.WaitTime());
        }
    }
}

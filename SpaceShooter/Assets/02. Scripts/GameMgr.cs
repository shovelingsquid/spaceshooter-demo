﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMgr : MonoBehaviour {

    public Transform[] points;
    public GameObject monsterPrefabs;
    public GameObject bossPrefabs;
    public List<GameObject> monsterPool = new List<GameObject>();

    public static GameMgr instance = null;

    private GameUI gameUI;

    public bool isGameOver = false;

    public float createTime = 2f;
    public int maxMonster = 10;
    private int madeMonster = 0;
    public float stageCoolTime = 5f;

    void Awake()
    {
        instance = this;
    }

    void Start ()
    {
        points = GameObject.Find("SpawnPoint").GetComponentsInChildren<Transform>();

        for(int i = 0; i < maxMonster; i++)
        {
            GameObject monster = (GameObject)Instantiate(monsterPrefabs);
            monster.name = "Monster_" + i.ToString();
            monster.SetActive(false);
            monsterPool.Add(monster);
        }

        GameObject boss = (GameObject)Instantiate(bossPrefabs);
        boss.name = "BOSS";
        boss.SetActive(false);
        monsterPool.Add(boss);

        if (points.Length > 0)
        {
            StartCoroutine(this.CreateMonster());
        }

        gameUI = GameObject.Find("GameUI").GetComponent<GameUI>();
    }
	
	IEnumerator CreateMonster()
    {
        while(!isGameOver)
        {
            //int monsterCount = (int)GameObject.FindGameObjectsWithTag("MONSTER").Length;

            //if (monsterCount < maxMonster)
            //{
            //    yield return new WaitForSeconds(createTime);

            //    int idx = Random.Range(1, points.Length);
            //    Instantiate(monsterPrefabs, points[idx].position, points[idx].rotation);
            //}
            //else
            //{
            //    yield return null;
            //}

            yield return new WaitForSeconds(createTime);

            foreach (GameObject monster in monsterPool)
            {
                if (!monster.activeSelf)
                {
                    int idx = Random.Range(1, points.Length);
                    monster.transform.position = points[idx].position;
                    monster.SetActive(true);
                    madeMonster++;
                    break;
                }
            }

            if (gameUI.isBoss)
            {
                int idx = Random.Range(1, points.Length);

                Instantiate(bossPrefabs, points[idx].position, points[idx].rotation);
                Debug.Log("Boss Create");
                yield break;
            }

            if (isGameOver || gameUI.maxCount == madeMonster)
            {
                yield break;
            }
                
        }
    }

    public IEnumerator WaitTime()
    {
        madeMonster = 0;

        float i = stageCoolTime;

        while (i > 0)
        {
            i -= 1 * Time.smoothDeltaTime;
            yield return null;
        }

        StartCoroutine(this.CreateMonster());
        yield break;
    }
}

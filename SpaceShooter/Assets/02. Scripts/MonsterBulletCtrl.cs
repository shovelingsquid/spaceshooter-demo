﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterBulletCtrl : MonoBehaviour {
    public int damage = 5;
    public float speed = 500.0f;
    // Use this for initialization

    private UIMgr UIDATA;

    void Start () {
        GetComponent<Rigidbody>().AddForce(transform.forward * speed);
	}
	
	// Update is called once per frame
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallCtrl : MonoBehaviour {


    void OnCollisionEnter(Collision coll)
    {
        if(coll.collider.tag == "BULLET" || coll.collider.tag == "MBULLET")
        {
            Destroy(coll.gameObject);
        }
    }

    void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.tag == "MBULLET")
        {
            Destroy(coll.gameObject);
        }
    }
}

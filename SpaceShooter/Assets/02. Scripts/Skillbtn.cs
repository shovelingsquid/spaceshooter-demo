﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Skillbtn : MonoBehaviour {
    public Image FillImage;
    public Text coolTimeCounter;

    public float coolTime;
    private float currentCoolTime;

    private bool canUseSkill = true;

    // Use this for initialization
    void Start ()
    {
        FillImage.fillAmount = 0;
	}
	
    public void UseSkill()
    {
        if(canUseSkill)
        {
            FillImage.fillAmount = 1;
            StartCoroutine("CoolTime");

            currentCoolTime = coolTime;
            coolTimeCounter.text = "" + currentCoolTime;

            StartCoroutine("CoolTimeCounter");

            canUseSkill = false;
        }
        else
        {
            //사용불가
        }
    }

    IEnumerator CoolTime()
    {
        while(FillImage.fillAmount > 0)
        {
            FillImage.fillAmount -= 1 * Time.smoothDeltaTime / coolTime;
            yield return null;
        }

        canUseSkill = true;
        yield break;
    }

    IEnumerator CoolTimeCounter()
    {
        while(currentCoolTime > 0)
        {
            yield return new WaitForSeconds(1.0f);

            currentCoolTime -= 1.0f;
            coolTimeCounter.text = "" + currentCoolTime;
        }
        yield break;
    }
}
